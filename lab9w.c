#include<stdio.h>
void swap(int *a,int *b)
{
int temp;
temp=*a;
*a=*b;
*b=temp;
}
int main()
{
int n1,n2;
printf("enter two numbers: ");
scanf("%d%d",&n1,&n2);

printf("before swaping: \n");
printf("\n%d",n1);
printf("\n%d\n",n2);

swap(&n1,&n2);

printf("\nafter swaping: \n");
printf("\n%d",n1);
printf("\n%d",n2);
return 0;
}
